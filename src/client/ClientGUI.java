package client;

import javax.swing.*;
import java.applet.Applet;
import java.awt.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

class ClientGUI extends JFrame {
    private final static int MIN_WIDTH = 770;
    private final static int MIN_HEIGHT = 540;

    private GameStub stub;

    ClientGUI() {
        super("Old School Runescape");
        this.stub = new GameStub();
        setMinimumSize(new Dimension(MIN_WIDTH, MIN_HEIGHT));
    }

    void run() {
        try {
            Applet applet = (Applet) getClazz().getDeclaredConstructor().newInstance();
            applet.setSize(800, 600);
            applet.setStub(this.stub);
            applet.init();
            applet.start();
            this.add(applet);
            setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Class<?> getClazz() {
        try {
            URL[] url = new URL[]{new URL("file:" + stub.getFilePath())};
            URLClassLoader urlClassLoader = new URLClassLoader(url);
            return urlClassLoader.loadClass("client");
        } catch ( MalformedURLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
