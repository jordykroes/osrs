package client;

import javax.swing.filechooser.FileSystemView;
import java.applet.AppletContext;
import java.applet.AppletStub;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class GameStub implements AppletStub {
    private Map<String, String> parameters;
    private String filePath;

    GameStub() {
        parseParameters();
        downloadGamepack();
    }

    private void parseParameters() {
        Map<String, String> config = new HashMap<>();

        try {
            final InputStream inputStream = new URL("http://oldschool60.runescape.com/jav_config.ws").openStream();
            final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;

            while ((line = reader.readLine()) != null) {
                final String[] split;

                if (line.startsWith("msg")) {
                    split = line.split("=", 3);
                    config.put(split[1], split[2]);
                } else if (line.startsWith("param")) {
                    split = line.split("=", 3);
                    config.put(split[1], split[2]);
                } else {
                    split = line.split("=", 2);
                    config.put(split[0], split[1]);
                }
            }
            this.parameters = config;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void downloadGamepack() {
        final String codebase = getParameter("codebase");
        final String initialJar = getParameter("initial_jar");
        final InputStream inputStream;
        final FileOutputStream outputStream;
        filePath = FileSystemView.getFileSystemView().getDefaultDirectory().getPath() + "gamepack.jar";

        try {
            inputStream = new URL(codebase + initialJar).openStream();
            outputStream = new FileOutputStream(new File(this.filePath));
            int data;

            while ((data = inputStream.read()) != -1) {
                outputStream.write(data);
            }

            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    String getFilePath() {
        return this.filePath;
    }

    @Override
    public boolean isActive() {
        return false;
    }

    @Override
    public URL getDocumentBase() {
        try {
            return new URL(getParameter("codebase"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public URL getCodeBase() {
        try {
            return new URL(getParameter("codebase"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getParameter(String name) {
        return parameters.get(name);
    }

    @Override
    public AppletContext getAppletContext() {
        return null;
    }

    @Override
    public void appletResize(int width, int height) {}
}
